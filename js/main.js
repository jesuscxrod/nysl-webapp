// Globals

var menu_header = document.getElementById('menu-header');
var menu_nav = document.getElementById('menu-nav');
var filter_menu = document.getElementById('filter');
var arrow_menu = document.getElementById('arrow')
var main = document.getElementById('main');
var game_one = document.getElementById('game-one');
var game_two = document.getElementById('game-two');
var match_one = document.getElementById('match-one');
var match_two = document.getElementById('match-two');
var menu_page = document.getElementById('menu-page');
var title = document.getElementById('title');
var maps = document.getElementsByName('iframe');


//hidden pages
match_one.style.display = 'none';
match_two.style.display = 'none';
arrow_menu.style.display = 'none';

//Events listeners
arrow_menu.addEventListener("click", ()=>{
    match_one.style.display = 'none'
    match_two.style.display = 'none'
    game_one.style.display = ''
    game_two.style.display = ''
    arrow_menu.style.display = 'none'
    filter_menu.style.display = ''
    title.style.display = ''
})

if (window.matchMedia("(orientation: portrait)").matches) {
    game_one.addEventListener("click", ()=>{
        match_one.style.display = ''
        filter_menu.style.display = 'none'
        arrow_menu.style.display = ''
        game_one.style.display = 'none'
        game_two.style.display = 'none'
        title.style.display = 'none'
    })
    
    game_two.addEventListener("click", ()=>{
        match_two.style.display = ''
        filter_menu.style.display = 'none'
        arrow_menu.style.display = ''
        game_one.style.display = 'none'
        game_two.style.display = 'none'
        title.style.display = 'none'
    })
 }

 if (window.matchMedia("(orientation: landscape)").matches) {

    maps[0].setAttribute("height", "100vh")

    game_one.addEventListener("click", ()=>{
    match_one.style.display = ''
    filter_menu.style.display = 'none'
    arrow_menu.style.display = ''
    game_one.style.display = 'none'
    game_two.style.display = 'none'
    title.style.display = 'none'
    })

 }


window.addEventListener("orientationchange", function() {
    location.reload()
  }, false);